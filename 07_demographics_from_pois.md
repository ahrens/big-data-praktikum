﻿# Lernen demographischer Informationen über Personen aus der Point-of-interest (POI) Historie

## Motivation

Check-in Apps wie Foursquare oder Brightkite bieten die Möglichkeit, nach interessanten Orten (points of interest, POI) zu suchen und dort einzuchecken. Mit der App lässt sich bspw. nach einem Restaurant in der Nähe suchen und im Anschluss nach erfolgtem Check-in ggf. ein Preisnachlass erhalten. Check-in Apps geben außerdem personalisierte Empfehlungen für interessante Orte und ermöglichen den Nutzenden, sich mit Freunden zu verknüpfen und Tipps über Orte zu geben. Solche Check-in Daten sind jedoch höchst sensitiv, da sie Standort-Informationen und private Attribute einer Person preisgeben können, bspw. wo diese Person wohnt, arbeitet oder welcher Religion sie angehört. Um das Privatsphäre-Risiko von Check-in Daten zu erforschen, soll untersucht werden, ob es möglich ist, auch demographische Attribute einer Person aus deren Check-in-Historie zu lernen, wie zum Beispiel deren Alter oder Geschlecht.

## Zielstellung

Die Aufgabe des Projektes ist es, mithilfe von Machine Learning einen Klassifikator mit ausreichender Genauigkeit zu konstruieren, der anhand der Check-in-Historie einer Person deren Geschlecht (bzw. falls verfügbar, weitere demographische Informationen, wie z.b. das Alter) erkennt. Dazu sollen zwei öffentlich verfügbare Check-in Datensätze untersucht sowie ein dritter Datensatz aus mehreren Quellen verknüpft werden.

## Arbeitspakete

### 1. Datenvorverarbeitung
Zur Bearbeitung der Aufgabe soll der Datensatz LifeSpec von Microsoft Research Asia verwendet werden, der die POI-Historie in Form von Check-in Daten und das Geschlecht der Nutzenden einer Social Media App enthält. Der zweite zu untersuchende Datensatz stammt von Foursquare und enthält ebenfalls Check-in Daten von Nutzenden der Foursquare-App. Der dritte Datensatz soll eigenständig erstellt werden. Dazu sollen Profildaten eines öffentlichen Datensatzes der Dating-App 'OK Cupid' auf Identifikatoren untersucht werden, die das Profil zu einer weiteren Social Media Platform flickr linken lassen. flickr ist eine Foto-Sharing Website, auf der Nutzende Fotos teilen, wobei diese Fotos auch Standort-Informationen und Zeitstempel enthalten und somit eine POI-Historie darstellen. Fotos und Profilinformationen von Nutzenden von flickr sind einerseits in einem öffentlichen Datensatz sowie per API unter Berücksichtigung der entsprechenden Richtlinien der Seite verfügbar. Es soll aus diesen Datenquellen ein Datensatz erstellt werden, der pro Nutzendem dessen POI-Historie sowie demographische Attribute wie Geschlecht, Alter, Bildung etc. enthält.

### 1. Modellierung
Anschließend soll ein für das Problem geeignetes Machine-Learning-Modell ausgewählt sowie eine passende ML-Bibliothek gefunden werden, in welcher der entsprechende Algorithmus auf Basis der Daten trainiert und dessen Klassifikationsgenauigkeit evaluiert werden kann. Hierbei sind Precision, Recall und Accuracy zu bestimmen. Die Auswertung soll für alle drei Datensätze erfolgen.


### 3. Vortrag
Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze (Modell- und Merkmalsauswahl) und die Ergebnisse dargestellt werden. 


## Literatur & Datensätze

LifeSpec
- dataset: https://miro.com/app/board/uXjVPgLKY8g=/?moveToWidget=3458764548621148255&cot=14
- Yuan et al. 2013. We know how you live: exploring the spectrum of urban lifestyles. https://doi.org/10.1145/2512938.2512945

Foursquare
- dataset: https://sites.google.com/site/yangdingqi/home/foursquare-dataset

flickr
- dataset: https://lbsn.vgiscience.org/yfcc-introduction/, https://github.com/visipedia/fg_geo
- API: https://www.flickr.com/services/api/, https://github.com/alexis-mignon/python-flickr-api
- Bai et al. 2022. Heri-Graphs: A Workflow of Creating Datasets for Multi-modal Machine Learning on Graphs of Heritage Values and Attributes with Social Media: https://miro.com/app/board/uXjVPgLKY8g=/?moveToWidget=3458764548311043965&cot=14

OK Cupid
- dataset: https://www.kaggle.com/datasets/andrewmvd/okcupid-profiles
