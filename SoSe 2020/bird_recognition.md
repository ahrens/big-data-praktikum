# Bird Voice Recognition

## Motivation

Alle Vogelarten weisen charakteristische Laute und individuellen Gesang auf, was durch bestimmte Muster im Klangprofil (Spektrogramm) erkennbar ist. Infolgedessen lassen sich für ein gegebenes Klangprofil (Audio-Datei) verschiedene Fragestellungen formulieren:

- Welche Vogelart ist (vordergründig) zu hören?
- Wie viele Vögel singen?
- Was ist der Zweck des Gesangs?
  - Warnruf
  - Revierruf (Verteidung, Drohung)
  - Balzgesang
  - Antwortgesang (Kommunikation / Interaktion zwischen Vögeln)
- In welcher Stimmung ist ein Vogel?

Die Nutzung von Bilddaten ist für die Erkennung der Vogelart weniger geeignet, da Vögel sich sehr schnell bewegen, gut getarnt sind und sich oft in Bäumen oder Sträuchern aufhalten.

Durch die automatische Beantwortung der genannten Fragestellungen durch Machine-Learning-Verfahren profitiert vor allem die biologische Forschung. Daneben unterstützen derartige Anwendungen auch bei der Überwachung von Vogelpopulation, beispielsweise im Rahmen von ökologischen Fragestellungen. 

Die automatische Klassifikation von Vogelarten wird jedoch auch durch verschiedene Herausforderungen erschwert. Einerseits enthalten Audio-Daten oft Störgeräusche, wie z.B. Verkehrslärm oder Wind, was die Erkennung der Vogelart erschwert. Andererseits gibt es eine große Anzahl an Vogelarten (allein in Deutschland mehr als 400), welche mitunter auch gleichzeitig in einer Aufnahme zu hören sein können.


## Zielstellung

Ziel des Themas ist die Entwicklung einer Anwendung zur automatischen Erkennung von Vogelarten basierend auf dem Gesang. Die Anwendung soll dazu Audio-Dateien als Eingabe erhalten. Das Kernproblem besteht darin, einen geeigneten Machine-Learning-Ansatz zu ermitteln und umzusetzen, welcher mit Hilfe von (bereits gelabelten) Testdaten trainiert werden soll. Schließlich soll eine Evaluierung durchgeführt werden, wo ersichtlich wird, mit welcher Zuverlässigkeit die Vogelart bestimmt werden kann. Hierbei ist von Interesse, welche Auswirkungen verschiedene Ansätze, Parameter sowie Trainings- und Test-Daten auf die Qualität der Vorhersage haben.


## Arbeitspakete

### 1. Datenextraktion & Lösungsskizze
- Datenextraktion: Zunächst muss ein geeigneter Datensatz ausgewählt werden. Hierzu eignet sich beispielsweise [Xeno-Canto](https://www.xeno-canto.org/), welche viele Audio-Aufnahmen über eine API zu Verfügung stellt. An dieser Stelle sollte das Problem vereinfacht werden, indem zunächst z.B. nur Audio-Aufnahmen für die 20 häufigsten in Deutschland vorkommenden Vogelarten mit hoher Qualität ausgewählt werden. Je Vogelart kann außerdem die Menge an Audio-Aufnahmen beschränkt werden (z.B. 50). Später kann dann evaluiert werden, wie die Qualität der Vorhersage bei Zunahme der Trainingsdaten verbessert werden kann. 
- Lösungskizze: Zur Lösung des Problem ist es notwendig eine Literaturrecherche durchzuführen, um abzuschätzen, welche ML-Verfahren sich für die Lösung des Problems eignen. Darüber hinaus sollte eine ML-Bibliothek gewählt werden und eine Einarbeitung darin erfolgen.

### 2. Implementierung
- Basierend auf der Lösungsskizze ist das ML-Verfahren zu implementieren. Hierbei ist darauf zu achten, dass Trainings- und Testdaten sowie Parameter flexibel ausgetauscht werden können, sodass eine einfache Evaluierung gewährleistet ist. Nachdem die Funktionalität gewährleistet ist, kann noch ein weiteres Verfahren als Vergleichsgrundlage implementiert werden.

### 3. Evaluierung
- Das umgesetzte Verfahren ist hinsichtlich der Vorhersagequalität zu beurteilen. Hierzu eignen sich die Metriken Recall, Precision und F-Measure. Es ist zu untersuchen, inwieweit die Aufteilung von Trainings- und Testdaten, deren Umfang sowie verschiedene Parameter die Vorhersagequalität beeinflussen.

### 4. Vortrag
- Ausarbeit einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze (kurze Beschreibung des Algorithmus, Nennung der verwendeten Bibliotheken) und die Ergebnisse der Evaluierung dargestellt werden. 

## Weiterführende Arbeiten

Neben der Erkennung der Vogelart kann das Verfahren bezüglich weiterer Fragestellungen erweitert werden. Beispielsweise kann neben der Vogelart auch die Art des Gesangs (Warnruf, Balzgesang, ...) vorhergesagt werden. Interessant ist auch die Ermittlung, ob überhaupt eine Vogelstimme in einer gegebenen Audio-Aufnahem vorhanden ist.


## Literatur

- [Bird Detection in Audio](https://ieeexplore.ieee.org/abstract/document/7738875)
- [Automatic acoustic detection of birds through deep learning: The first Bird Audio Detection challenge](https://besjournals.onlinelibrary.wiley.com/doi/abs/10.1111/2041-210X.13103)
- [On-Bird Sound Recordings: Automatic Acoustic Recognition of Activities and Contexts](https://ieeexplore.ieee.org/document/7933044)
- [Audio Based Bird Species Identification using Deep Learning Techniques](https://infoscience.epfl.ch/record/229232/files/16090547.pdf)
- [Large-ScaleBird SoundClassificationusing Convolutional Neural Networks](https://www.researchgate.net/profile/Danny_Kowerko/publication/322144806_Large-Scale_Bird_Sound_Classification_using_Convolutional_Neural_Networks/links/5ad4bfe2a6fdcc2935808d8e/Large-Scale-Bird-Sound-Classification-using-Convolutional-Neural-Networks.pdf)
- [Automatic large-scale classification ofbird sounds is strongly improved byunsupervised feature learning](https://peerj.com/articles/488.pdf)
- [Deep Learning for Audio Event Detection and Tagging on Low-Resource Datasets](https://www.mdpi.com/2076-3417/8/8/1397)
- [Automatic acoustic identification of individual animals: Improving generalisation across species and recordingconditions](https://arxiv.org/pdf/1810.09273.pdf)

## Siehe auch

- [DCASE 2018: Bird Audio Detection Challenge](http://dcase.community/challenge2018/task-bird-audio-detection)
- [Warblr App](https://www.warblr.co.uk/)