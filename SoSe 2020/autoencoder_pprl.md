# Autoencoder für Bloom-Filter

## Motivation
Privacy Preserving Record Linkage (PPRL) befasst sich mit der Integration von sensitiven Daten, wie z. B. Patientendaten. 
Das Ziel dieser Verfahren ist die Identifikation von Datensätzen, welche dieselbe Realweltentität repräsentieren und von verschiedenen Parteien, 
wie z. B. Krankenhäuser, Krankenkassen oder Gesundheitsämter, stammen. Personenbezogene Daten sind sensitiv, dass heißt, die Daten müssen geschützt werden 
und dürfen nicht in Klartext an andere Parteien übermittelt werden. Um dennoch eine Integration zu ermöglichen, 
werden ausgewählte Attribute der Datensätze codiert (maskiert), so dass der Attributwert nicht mehr erkennbar ist. 
Die drei wesentlichen Herausfoderungen im Rahmen von PPRL sind Skalierbarkeit auf großen Datenmenge mit mehreren Millionen Datensätzen, 
dass Erreichen einer hohen Linkage-Qualität sowie die Einhaltung der Privatsphäre. 
Für die Codierung werden im Allgemeinen Bloom-Filter verwendet (siehe Abb. 1), so dass Rückschlüsse auf die realen Daten verhindert
werden sollen. 

| ![bloom filter](pprl/pprl_bloom_filter.PNG "Bloom Filter") | 
|:--:|
| Beispiel für die Erstellung eines Bloom Filters mittel N-Grammen und 2 Hashfunktionen. Jede Hash-Funktion bildet auf eine Bit-Position eines Bit-Vektors ab. |

Die Bloom-Filter werden von jedem Datenbesitzer generiert und zu einer Linkage-Unit gesendet, welche die Berechnung der Duplikate durchführt. Um Duplikate zu bestimmen wird
für jedes Bloom-Filter Paar eine Ähnlichkeit bestimmt, wie z.B. Jaccard-Similarity, bei der das Verhältnis der gemeinam gesetzten Bits und die Gesamtanzahl aller 
gesetzten Bits gebildet wird.

Jedoch existieren bereits Ansätze, die aufgrund der Häufigkeit der gesetzten Positionen im Bloom-Filter und der realen Häufigkeit 
von bestimmten Buchstabenkombinationen Rückschlüsse auf die ursprünglichen Daten ziehen können.



## Zielstellung
Es soll evaluiert werden, ob mittels Autoencoder Bloom Filter zu Embeddings transformiert werden können, so dass zum Einen Häufigkeitsanalysen nicht möglich sind und zum Anderen 
die Qualität erhalten bleibt beim Record-Linkage. Autoencoder sind neuronale Netze, wobei das Ziel ist die Dimension der Daten zu reduzieren. Dies wird als Encoding bezeichnet. 
Die reduzierte Repräsentation wird als Hidden Layer bezeichnet. Durch einen Decoder werden die Eingabedaten basierend auf der kompakten und reduzierten Repräsentation 
rekonstruiert. Autoencoder sind dem Unsupervised Learning zuzuordnen, dass heißt es werden keine gelabelten Trainingsdaten benötigt.

| ![Autoencoder](pprl/autoencoder.PNG "Autoencoder") |
|:--:|
| Quelle: https://ramhiser.com/post/2018-05-14-autoencoders-with-keras/ |

## Arbeitspakete

1. <b> Modellerstellung</b><br>
  Beim ersten Schritt soll ein Autoencoder Modell mittels Keras erstellt werden. Hierfür können Sie auf bereits exisitierende Methoden zurückgreifen, wie z.B. LSTM-Autoencoder,
CNN-Autoencoder, etc. Dabei sollen verschiedene Hidden Layer Größen evaluiert werden. Weiterhin sollen bzgl. der Architektur ebenfalls die verschiedenen Parameter beachtet werden.
    
2. <b> Anwendung</b><br>
   Das Record-Linkage soll durch den Vergleich der Hidden Layer Repräsentationen mittels der Kosinusähnlichkeit erfolgen. Diesbezüglich sollen verschiedene Threshold getestet werden. 
    Die reduzierten Daten sollen auf zwei Arten generiert werden, gemeinsames Modell vs pro Datenquelle.
3.  <b>Evaluation</b><br>
    Alle Konfigurationen sollen bzgl. des resultierenden Matchergebnis evaluiert werden mittels Precision, Recall und F-Measure. 
4. <b>Vortrag</b><br>
   Ausarbeit einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze (kurze Beschreibung des Algorithmus, 
   Nennung der verwendeten Bibliotheken) und die Ergebnisse der Evaluierung dargestellt werden.

## Datensätze
* generierte Daten und NorthCarolina Voter Datensatz

## Links
* [Building Autoencoders in Keras](https://blog.keras.io/building-autoencoders-in-keras.html)
* [LSTM Autoencoder implementation](https://github.com/basma-b/sentence_autoencoder_keras)
* [CNN based Autoencoder implementation](https://github.com/UmeanNever/CNN_DCNN_text_classification)