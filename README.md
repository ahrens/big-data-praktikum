# Big-Data Praktikum

## Übersicht


Das Praktikum beinhaltet den Entwurf und die Realisierung einer Anwendung oder eines Algorithmus im Big-Data und Machine-Learning-Umfeld. In der Regel erfolgt die Implementierung unter Verwendung von Big-Data oder Deep-Learning-Frameworks. 

<!--break-->

Im einzelnen sind folgende Teilaufgaben zu lösen:

1. <b>Konzeptioneller Entwurf</b>. Es ist ein Entwurfsdokument anzufertigen, welches konzeptionell den Ablauf und die Architektur ihrer Anwendung darstellt. Im Dokument muss ersichtlich werden, welches Ziel verfolgt wird und welche Aufgaben dafür zu bewältigen sind. Das Dokument soll sich vom Umfang auf ca. 4 Seiten beschränken.
2. <b>Implementierung</b>. Basierend auf ihrem Entwurf soll die Anwendung realisiert werden. Das Resultat dieser Phase ist ein dokumentiertes, ausführbares Programm.
3. <b>Abschlusspräsentation</b> Am Ende des Praktikums stellt jede Gruppe ihr Projekt vor, wobei sie ihre Anwendung beschreibt sowie die Resultate präsentiert. Die Dauer der Präsentation soll ca. <b>10 Minuten</b> (+ 5 min. Diskussion) betragen.


## Anmeldung und Präsenzveranstaltungen


* Die Anmeldung zum Praktikum erfolgt über <a href="https://almaweb.uni-leipzig.de/" target = "_blank">Almaweb</a>. 
  * Bei Fragen und Problemen zur Anmeldung wenden Sie sich bitte immer an das Studienbüro via einschreibung(at)math.uni-leipzig.de

* Für die endgültige Anmeldung und Themenzuordnung melden Sie sich im Moodle unter folgender Adresse an:
<a href='https://moodle2.uni-leipzig.de/course/view.php?id=31409'> Anmeldung Moodle</a><br>
Sie haben bis zum 18.04. Zeit Ihre präferierten Themen und Wunschpartner anzugeben.

* <b>Vorbesprechung</b>
  * Erste Mailkommunikation für die Konkretisierung des Themas und die ersten Schritte werden mit dem Betreuer per Mail abgehalten.
  


## Testate


Das Praktikum gliedert sich in drei Teile. Nach jeder der drei Teilaufgaben wird eine Dokumentation oder ein Testat per Skype/Slack/ oder ähnliches durchgeführt. 
Die Art der Durchführung soll mit dem Betreuer individuell abgesprochen werden.
Zum erfolgreichen Absolvieren des Praktikums müssen <b>alle</b> drei Testate erfolgreich abgelegt werden. 
Wird ein Termin nicht eingehalten, verfallen die bereits erbrachten Teilleistungen. 
Die konkreten Termine für die ersten zwei Testate bzw. Abgabe sind mit dem Betreuer per E-Mail zu vereinbaren. 
Es gelten die nachfolgenden Fristen:

* Testat 1 (Entwurf): Ende Mai
* Testat 2 (Realisierung): Mitte-Ende Juli
* Testat 3 (Präsentation): tba


## Themen

<strong class="aktuell">Bitte vereinbaren Sie zeitnah einen Termin mit Ihrem Betreuer zur Besprechung der ersten Schritte!</strong>


| Nr | Thema | Betreuer | Technologie| Studenten | Material |
| -- |-------|----------|------------|-----------|----------|
| 01 | [Film- und Serien Temporal Graph (Netflix, Disney+, Prime, Hulu)](01_temporal_graph.md) | [C. Rost](https://dbs.uni-leipzig.de/person/christopher_rost) | Java |  | -- |
| 02 | [Synthetische Zeitreihen](02_synthdata.md) | [T. Burghardt](https://dbs.uni-leipzig.de/person/thomas_burghardt) | Python |  | -- |
| 03 | [Analyse und Vorhersage städtischer Emissionen](03_city_emissions.md) | [L. Lange](https://dbs.uni-leipzig.de/person/lucas_lange) | Python |  | [Datensatz-Paper](https://www.nature.com/articles/sdata2018280) |
| 04 | [LID-DS to Beagle](04_LID-DS_to_Beagle.md) | [M. Grimmer](https://dbs.uni-leipzig.de/person/martin_grimmer) | Python |  | -- |
| 05 | [Playlist Link Prediction](05_playlist_link_prediction.md) | [D. Obraczka](https://dbs.uni-leipzig.de/person/daniel_obraczka) | Python |  | -- |
| 06 | [Autoncoder auf Human Cell Atlas-Daten](06_autoencoder.md) | [J. Ewald](https://scads.ai/scads-ai-team/), [M. Joas](https://scads.ai/scads-ai-team/)  | Python |  | -- |
| 07 | [Lernen demographischer Informationen über Personen aus der Point-of-interest (POI) Historie](07_demographics_from_pois.md) | [M. Schneider](https://dbs.uni-leipzig.de/person/maja_schneider) | Python |  | -- |
| 08 | [Effiziente Extraktion von Teildatensätzen aus MongoDB](08_efficient_ncvr_derivation_from_mongodb.md) | [F.Rohde](https://dbs.uni-leipzig.de/person/florens_rohde) | Python/Java |  | -- |
| 09 | []() |  |  |  | -- |
| 10 | []() |  |  |  | -- |
| 11 | []() |  |  |  | -- |
| 12 | []() |  |  |  | -- |
| 13 | []() |  |  |  | -- |
| 14 | []() |  |  |  | -- |



Teilnehmerkreis


Master-Studiengänge Data-Science und bei freien Plätzen auch Master-Studiengänge Informatik.  Die Teilnahme erfolgt in 2er-Gruppen, 
<strong class="aktuell">die Teilnehmerzahl ist auf ca. 24 Studierende beschränkt!</strong>. 
 Zu beachten ist, dass Studenten, die das Big-Data-Praktikum noch nicht belegt haben, bevorzugt werden. 

## Erwartete Vorkenntnisse

* **Java**/**Python**/...-Kenntnisse (siehe Technologie in Tabelle)
* Vorlesung Cloud Data Management und NoSQL-Datenbanken hilfreich
* Linux-Kenntnisse von Vorteil
* Git-Kenntnisse von Vorteil
