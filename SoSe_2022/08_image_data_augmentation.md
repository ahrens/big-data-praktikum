# Image data augmentation

## Motivation

Deep convolutional neural networks have performed remarkably well on many Computer Vision tasks. However, these networks are heavily reliant on big data to avoid overfitting. Overfitting refers to the phenomenon when a network learns a function with very high variance such as to perfectly model the training data. Unfortunately, many application domains do not have access to big data, such as medical image analysis. Image data augmentation is a technique that can be used to artificially expand the size of a training dataset by creating modified versions of images in the dataset.

## Goal
Your task in this practicum is to implement a library for image data augmentation. This will also include evaluating the implemented approaches on selected computer vision tasks.


## Work packages

### 1 Concept 

Make yourself familiar with the general concepts of image data augmentation. For example: https://journalofbigdata.springeropen.com/articles/10.1186/s40537-019-0197-0, https://www.youtube.com/watch?v=mljRx81K1gY
Create an initial sketch for the architecture of your solution. Keep in mind that this system needs to be modular enough to encompass different augmentation strategies.

### 2 Implementation

Implement your application. Your code needs to be documented and tested to ensure future usability and correctness. At the end of this phase you should also run your system on benchmark datasets to see how well the implemented approaches perfom.

### 3 Presentation

The presentation (10 min + 5 min discussion) should showcase how your system works (architecture and basic ideas), as well as present the results. 

### Literature & Links

- [survey paper](https://journalofbigdata.springeropen.com/articles/10.1186/s40537-019-0197-0)
- [youtube video](https://www.youtube.com/watch?v=mljRx81K1gY)
- [data augmentation in python](https://neptune.ai/blog/data-augmentation-in-python)
