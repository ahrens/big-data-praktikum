# Privacy-Preserving Sepsis Prediction with Federated Learning

**Voraussetzung**: Python-Programmierkenntnisse

## Motivation

Eine der größten Herausforderungen von medizinischer Datenanalyse besteht darin, dass maschinelles Lernen eine große Menge an Daten voraussetzt, während medizinische Daten hochsensibel und somit schwer zugänglich sind. 
Federated Learning (FL) ist eine Methode, mit der das Training eines Algorithmus auf mehreren dezentralen Edge-Geräten oder Servern mit lokalen Datenteilmengen durchgeführt wird, ohne dass ein Datenaustausch zwischen den Knoten stattfindet. 
Lediglich die Gewichte der Modelle werden geteilt und damit ein zentrales Modell optimiert. 
So kann Maschinelles Lernen betrieben werden, ohne dass sensible Rohdaten geteilt werden müssen, dementsprechend ist das Training des Klassifikators Privatsphäre-erhaltend.

In diesem Projekt soll die gemeinsame Verarbeitung von medizinischen Daten aus mehreren Krankenhäusern oder Institutionen simuliert werden. 
Es soll bestimmt werden, ob ein Patient mit hoher Wahrscheinlichkeit eine Sepsis bekommen wird oder nicht, damit frühzeitig entsprechende Gegenmaßnahmen getroffen werden können. 
Dabei werden Daten aus dem MIMIN III Datensatz verwendet, der klinische Daten von über 40.000 Patienten enthält.

## Zielstellung

Es soll ein Modell verteilt trainiert werden, das bestimmen kann, welche Patienten mit hoher Wahrscheinlichkeit an einer Sepsis erkranken.  
Dafür müssen die Daten in sinnvollen Teilmengen unterteilt und entsprechend der hinterlegten timestamps vorverarbeitet werden: zum Training sollen nur Daten von Patienten kurz vor oder bei der Einlieferung ins Krankenhaus verwendet werden.
Dann wird entweder ein Machine Learning Modell (SVM, RF, MLP) oder ein Deep Learning Modell (LSTM) trainiert. 
Dabei trainiert jeder Knoten das Modell auf seiner jeweiligen Teilmenge an Daten und spielt das Modell (local model) an einen Server zurück. Hier wird dann, je nach Algorithmus (FedSGD oder FedAvg), ein Zielmodell (global model) optimiert.  

## Arbeitspakete

__1 - Anwendungsdesign und Lösungsskizze__

Es ist eine Lösungsskizze zu erstellen, in welcher das genaue Vorgehen skizziert wird. 
Diese sollte beschreiben, wie die Trainingsdaten vorverarbeitet werden, welche Modelle implementiert werden, das verteilte Lernen funktioniert und die abschließende Evaluation aussieht.



__2 - Implementierung und Evaluation__

Basierend auf der Lösungsskizze sind die Daten aufzubereiten und die Modelle zu implementieren. 

Die Implementierung des verteilten Lernens kann beispielsweise mit den Python Bibliotheken PySyft, TensorFlow Federated oder OpenFL umgesetzt werden, aber auch andere Ansätze können verfolgt werden. 

Die Evaluation erfolgt anschließend mithilfe eines separaten Validierungsdatensatzes, der im Schritt der Datenvorverarbeitung bestimmt wird. 
Dabei wird die Genauigkeit und Generalisierungsfähigkeit der Klassifikatoren überprüft.


__3 - Vortrag__

Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze und die Evaluationsergebnisse vorgestellt werden.



## Literatur & Links

- [Mimic Dataset] https://mimic.mit.edu/
- [Federated Learning Overview Paper] https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=9084352
- [PySyft] https://github.com/OpenMined/PySyft
- [OpenFL] https://github.com/intel/openfl
- [TensorFlow Federated] https://www.tensorflow.org/federated

