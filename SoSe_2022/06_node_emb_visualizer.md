# Node Embedding und Graph-Visualizer

**Voraussetzung**: Python, Deep Graph Library

## Motivation

Im Bereich des Graph-Machine Learning ist ein integraler Bestandteil die Generierung von Knotenrepräsentationen 
auch Node-Embeddings genannt für die Anwedung weiterer ML-Aufgaben, wie z.B. Link-Prediction oder 
Node-Classification. Node-Embeddings werden als Vektoren dargestellt. Je nach Embedding-Verfahren und der 
Klassifikationsaufgabe können die Vektoren unterschiedlich im Vektorraum positioniert sein. 

Im Gegensatz dazu, existiert der urspüngliche Graph mit seiner Topologie basierend auf den Kanten. 
Weiterhin existiert das Wissen des Anwenders, welche Knoten ähnlich sein sollen oder welche in
Beziehung stehen sollen.

Für die Analyse der Embedding-Modelle und Konfiguration ist eine 2/3- dimensionale Darstellung hilfreich,
um potentielle Fehlkonfigurationen zu identifizieren, wenn z.B. benachbarte Knoten oder die in Beziehung 
stehen sollen weit entfernt im Vektorraum liegen 
## Zielstellung

Ziel des Themas ist die Entwicklung einer Visualisierung für Node-Embeddings und des dazughörigen Graphen.


Folgende Features sollen bei der Anwendung enthalten sein:

* Laden eines Graphen
* Auswahl eines bestehenden Node-Embedding Verfahren (in der Anwendung integriert)
* Trainieren eines Modells() mit konfigurierbaren Setting 
* Darstellung der node-Embeddings mittels t-SNE 
* Visualisierung des gesamten Graphen oder Ausschnitts eines Subgraphen ausgehend von einem selektierten node-embedding ausgehend.

Der Betreuer [Christopher Rost](https://dbs.uni-leipzig.de/person/christopher_rost) wird ihnen eine umfangreiche Einweisung in die Thematik geben.

## Arbeitspakete

__1 - Anwendungsdesign und Lösungsskizze__

Zur Lösung des Problems ist es notwendig sich mit [DGL](https://www.dgl.ai/) für die Generierung von Embeddings sowie deren Darstellung.
Für die Darstellung des Graphen können beliebige Bibliotheken verwendet werden. Einige sind 
[hier](https://towardsdatascience.com/visualizing-networks-in-python-d70f4cbeb259) aufgelistet.
Es ist eine Lösungsskizze anzufertigen, die beschreibt, anhand welcher Schritte die geforderten 
Anforderungen umzusetzen sind. 

__2 - Implementierung und Evaluation__

Basierend auf der Lösungsskizze ist die Visualisierung zu implementieren. Der Code soll dokumentiert und korrekt sein.
Am Ende der Implementierung soll die Anwendung lauffähig sein, sodass anhand eines Beispielgraphen verschiedene Modelle 
berechnet werden können und entsprechend die Node-Embeddings dargestellt werden können.


__3 - Vortrag__

Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze
(kurze Beschreibung der Architektur, Nennung der verwendeten Transformationen) und die Evaluationsergebnisse vorgestellt werden.


## Links

- [DGL](https://www.dgl.ai/)
- [Graph Visualisierungen in Python](https://towardsdatascience.com/visualizing-networks-in-python-d70f4cbeb259)