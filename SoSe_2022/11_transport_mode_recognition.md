﻿# Transport mode recognition

## Motivation

Bewegungsdaten von Personen sind höchst interessant für eine Vielzahl von Applikationen. Smartphones bieten installierten Apps die Möglichkeit, viele Sensordaten abzugreifen und auszuwerten, darunter auch GPS-Daten. Die Apps nutzen diese Informationen, z. B. um ihre Services ständig zu verbessern und bessere Nutzungserlebnisse anbieten zu können. Der Transportmodus ist hierbei ein interessanter Aspekt, der beispielsweise für Recommender-Systeme genutzt werden kann. Bei der Auswertung werden jedoch Daten abgerufen, die sensitiv sein können und möglicherweise die Privatsphäre des Smartphone-Tragenden gefährden können. 2013 haben de Montjoye et al. beispielsweise zeigen können, dass zusammenhängende Bewegungsdaten mit allein vier Datenpunkten ausreiched sein können, um eine Person zu identifizieren. Die Motivation für diese Aufgabenstellung ist daher, den Transportmodus einer Person auf Basis von GPS-Tracking-Daten zu erkennen und dabei allein mit Privatsphäre-unkritischen Informationen auszukommen.

## Zielstellung

Die Aufgabe des Projektes ist es, einen Klassifikator mit ausreichender Genauigkeit zur Erkennung des Transportmodus einer Person (z. B. Gehen, Fahrrad, Bus, Taxi/Auto etc.) zu konstruieren. Dabei sollen Privatsphäre-unkritische Merkmale verwendet sowie aus den Daten abgeleitet werden, um die Re-Identifizierung einer Person zu verhindern. Es soll eine Merkmalskombination gefunden werden, die sowohl ausreichende Klassifikationsgenauigkeit garantiert, als auch auf die Abfrage sensibler Attribute (beziehungsweise Attributkombinationen) verzichtet.



## Arbeitspakete

### 1. Datenvorverarbeitung, Featureengineering und Merkmalsselektion
Zur Bearbeitung der Aufgabe soll der Datensatz GeoLife von Microsoft Research Asia verwendet werden, der geotemporale Daten sowie entsprechende Transportmodus-Labels enthält. Die verfügbaren geotemporalen Merkmale sollen daraufhin untersucht werden, wie viel sie über eine Person verraten können. Hier sind insbesondere Merkmale wie GPS, Timestamp und der Routenverlauf kritisch zu betrachten. Weitere Merkmale, die für eine Klassifikation geeignet sein könnten, sollen aus den verfügbaren Daten abgeleitet werden. Hier könnten z. B. Merkmale untersucht werden wie Geschwindigkeit und Beschleunigung. Möglicherweise verraten Bewegungsmuster selbst etwas über den Transportmodus. Hier könnte eine Abstraktion des Bewegungsmusters basierend auf einer bestimmten Anzahl an aufeinanderfolgenden GPS-Koordinaten untersucht werden.
Es soll eine Korrellationsanalyse für die Merkmale mit dem Ziellabel erfolgen und basierend darauf einige Merkmalskombinationen ausgewählt werden (hier bieten sich visuelle Tools wie Orange an).

### 1. Modellierung
Anschließend soll ein für das Problem geeignetes Machine-Learning-Modell ausgewählt sowie eine passende ML-Bibliothek gefunden werden, in welcher der entsprechende Algorithmus auf Basis der Daten trainiert und dessen Klassifikationsgenauigkeit evaluiert werden kann. Hierbei sind Precision, Recall und Accuracy zu bestimmen. Die verschiedenen Merkmalskombinationen sollen mit dem gleichen Modellansatz evaluiert werden.


### 3. Vortrag
Ausarbeit einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze (Modell- und Merkmalsauswahl) und die Ergebnisse dargestellt werden. 


## Literatur

- Sadeghian, P., Håkansson, J. and Zhao, X. (2021) Review and evaluation of methods in transport mode detection based on GPS tracking data

## Links

- https://www.microsoft.com/en-us/research/publication/geolife-gps-trajectory-dataset-user-guide/
- https://orangedatamining.com/


