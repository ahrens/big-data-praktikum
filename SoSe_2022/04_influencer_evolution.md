# Influencer evolution in Sozialen Netzwerken

**Voraussetzung**: Java-Programmierkenntnisse

## Motivation

Graphen sind ein intuitives Datenmodell um komplexe Beziehungen zwischen Entitäten aus realen
Szenarios zu modellieren und zu analysieren. Ein typisches Beispiel für einen Graphen ist ein 
soziales Netzwerk, wo Nutzer als Knoten und Freundschaftsbeziehungen oder Nachrichten als Kanten
abgebildet werden. Da sich diese Objekte und Beziehungen in der Realität
über die Zeit verändern, verändern sich auch die modellierten Graphen bezogen auf deren
Struktur und Inhalt.

Aus der Graph-Theorie existieren Metriken, um die Wichtigkeit eines Knotens in einem Graphen abzuschätzen.
Eine Teilmenge dieser Metriken basiert auf den recht einfach zu berechnenden Degree des Knoten, 
was vereinfacht die Anzahl dessen ein- und ausgehenden Kanten ist. Die *degree centrality* sagt bspw. aus,
je mehr Verbindungen ein Knoten hat, desto wichtiger ist er für das Netzwerk. In sozialen Netzwerken 
könnte so bspw. ein Influencer detektiert werden.

Verändert sich der Graph über die Zeit, verändern sich jedoch auch die Knoten-degrees sowie die davon
abhängigen Metriken. Der nachfolgende Beispielgraph zeigt das Problem exemplarisch. 
Knoten und Kanten sind mit Gültigkeits-Intervallen annotiert. Es kann somit direkt gesehen werden, 
dass der Degree eines Knotens kein gleichbleibender Wert ist. Bspw. ist der Degree von A zum Zeitpunkt 1 genau 1, zum Zeitpunkt 3 jedoch 3.

![Example Graph](images/04_example_graph.png)

## Zielstellung

Ziel des Themas ist die Entwicklung von Analyse-Operatoren zur Berechnung von degree-basierten 
Metriken unter Verwendung des Graph-Processing Frameworks *Gradoop*, welches auf Apache Flink aufbaut. Weiterhin soll eine geeignete 
Visualisierung der Ergebnisse in Form eines Balken- oder Linien-Charts dargestellt werden. 
Die Skalierbarkeit der Implementierung ist ebenfalls zu untersuchen.

Folgende Metriken sollen temporal erweitert und implementiert werden:

* __Min/Max/Avg degree (vertex-centric)__: Der degree eines Knotens verändert sich über die Zeit. Für ein gegebenes Zeitintervall sollen min/max/avg degree eines Knotens ausgegeben werden.
* __Min/Max/Avg degree (graph-centric)__: Die degrees aller Knoten eines Graphen verändern sich über die Zeit. Für einen gegebenes Zeitintervall soll die Entwicklung des min/max/avg degrees des Graphen ausgegeben werden.
* __Degree range__: Der Degree-Range ist die Differenz zwischen größten und kleinsten Degree eines Graphen. Da sich beide Aggregate über die Zeit ändern, ändert sich auch der Degree Range. Die Entwicklung des Degree Range soll für ein gegebenes Zeitintervall ausgegeben werden.
* __Degree variance__: Eine Erweiterung des Degree Range, welche den Durchschnittlichen Degree einbezieht.
* __Average nearest neighbor degree (ANND)__: Neigt ein beliebter Nutzer dazu, sich zu anderen beliebten Nutzern zu verbinden? Dies kann mit den ANND herausgefunden werden. Pro Knoten werden die Degrees der Nachbarn betrachtet. Die Änderung dieser Metrik über die Zeit soll für einen gegeben Knoten und ein Zeitintervall ausgegeben werden.

Beispielcode eines Analyse-Operators:

```
public class MyOperator
  implements UnaryBaseGraphToValueOperator<TemporalGraph, DataSet<Tuple3<Long, Long, Integer>>> {
  /**
   * The time dimension that will be considered.
   */
  private final TimeDimension dimension;

  /**
   * The degree type (IN, OUT, BOTH);
   */
  private final VertexDegree degreeType;

  /**
   * Other configurations needed for your operator? Place your attributes here ...
   */

  /**
   * Constructor.
   */
  public MaxDegreeEvolution(VertexDegree degreeType, TimeDimension dimension, /** Some more params you need to configure your op **/) {
    this.degreeType = Objects.requireNonNull(degreeType);
    this.dimension = Objects.requireNonNull(dimension);
    ...
  }

  /**
   * Your operator gets a temporal graph as input and produces a DataSet of type Tuple3<Long, Long, Integer>
   */
  @Override
  public DataSet<Tuple3<Long, Long, Integer>> execute(TemporalGraph graph) {
    // Just an example how the flink workflow could look like
    return graph.getEdges()
      .flatMap(new FlatMapVertexIdEdgeInterval(dimension, degreeType)) // You _can_ reuse existing functions like this
      .groupBy(0)
      .reduceGroup(...)
      ...
      ;
  }
}
```

Als Beispieldaten werden ein Citibike-Datensatz sowie ein synthetisches soziales Netzwerk bereitgestellt. Die Evaluation wird auf unserem verteilten Cluster BDClu ausgeführt.

Die Libraries für eine geeignete Visualisierung können selbst gewählt werden. Beispiele wären ein Jupyter Notebook oder die JavaScript libraries d3 oder eCharts.

Der Betreuer [Christopher Rost](https://dbs.uni-leipzig.de/person/christopher_rost) wird ihnen eine umfangreiche Einweisung in die Thematik geben.

## Arbeitspakete

__1 - Anwendungsdesign und Lösungsskizze__

Zur Lösung des Problems ist es notwendig sich mit [Gradoop](https://github.com/dbs-leipzig/gradoop/wiki) sowie [Apache Flink](https://flink.apache.org/) und dessen [Dataset API](https://nightlies.apache.org/flink/flink-docs-release-1.14/docs/dev/dataset/overview/) vertraut zu machen.
Darüber hinaus sollte die Funktionsweise der genannten Graph Metriken für den statischen Fall verstanden werden. Diese sind [hier](http://olizardo.bol.ucla.edu/classes/soc-111/lessons-winter-2022/4-lesson-graph-metrics.html) nachzulesen.

Es ist eine Lösungsskizze anzufertigen, die beschreibt, anhand welcher Schritte die geforderten 
Metriken umzusetzen sind. Pro Metrik soll ein Gradoop Operator erstellt werden. Ein Operator 
erhält einen Graphen als Input und kann eine Collection von POJOs als Output liefern.

Weiterhin sollte sich in diesem Arbeitspaket auch für ein Visualisierungs-Tool/-Bilbliothek entschieden werden.


__2 - Implementierung und Evaluation__

Basierend auf der Lösungsskizze sind die Operatoren prototypisch zu implementieren. 
Die Implementierung findet in einem Feature-Branch von Gradoop statt. Bitte Forken sie zunächst das 
Gradoop Repository auf Github. Erstellen Sie dann einen neuen Branch ausgehend 
vom `develop` nach Absprache mit dem Betreuer. In diesem Branch findet die Entwicklung statt.

Die Evaluation der Implementierung erfolgt im Anschluss auf unserem verteilten Rechencluster. 
Dabei wird die Laufzeit und horizontale Skalierbarkeit untersucht.

__3 - Vortrag__

Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze
(kurze Beschreibung der Architektur, Nennung der verwendeten Transformationen) und die Evaluationsergebnisse vorgestellt werden.


## Literatur

- [Temporale Graph Analysen mit Gradoop](https://dbs.uni-leipzig.de/file/Rost2021_Article_DistributedTemporalGraphAnalyt.pdf)
- [Graph Metriken (statisch)](http://olizardo.bol.ucla.edu/classes/soc-111/lessons-winter-2022/4-lesson-graph-metrics.html)

## Links

- [Apache Flink DataSet API](https://nightlies.apache.org/flink/flink-docs-release-1.14/docs/dev/dataset/overview/)
- [Gradoop Wiki](https://github.com/dbs-leipzig/gradoop/wiki)
- [Gradoop Getting Started](https://github.com/dbs-leipzig/gradoop/wiki/Getting-started)
