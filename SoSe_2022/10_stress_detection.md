# Stress Detection From Wearables

**Voraussetzung**: Python-Kenntnisse empfohlen  
**Hinweis**: Literatur teilweise nur aus dem Uni-Netwerk kostenlos

## Motivation

Die Bestimmung des Geisteszustandes einer Person anhand physiologischer Daten kann in verschiedenen Bereichen eine entscheidenen Einblick gewähren. Eine große Rolle spielt unter Anderem die Erkennung von Emotionen bzw. Schmerzen in Locked-In Patienten, um eine Bessere Behandlung und Kommunkation zu ermöglichen (auf Wunsch auch als Projekt vorstellbar) [1]. Dieses Projekt konzentriert sich hingegen auf die Erkennung von Stress [2,3]. Im Sport aber auch Alltag finden sich dabei immer mehr Armbänder und Uhren für das laufende Monitoring von z.B. Herzfrequenz, Körpertempertur und Atmung. Die Integration von Stresserkennung in die Smart-Health Landschaft könnte den Auslösern von übermäßigem Stress und den ernsten Langzeitfolgen entgegenwirken. Die Aufgabe zielt daher speziell auf die mit einer Smartwatch auslesenbaren Daten ab, um die Effektivität einer Analyse anhand solcher Datenströme zu testen. Als Daten-Grundlage dient der WESAD Datensatz [4], welcher verschiedene physiologische Merkmale von 15 Personen mit ihren gefühlten Stressleveln verbindet. Das Stresslevel wird dabei in "neutral", "stress", und "amusement" unterschieden, wobei hier im Anschluss nur gestresst und nicht gestresst betrachtet werden soll.

## Zielstellung

Das zu erstellende Modell soll den Zustand einer Person anhand von physiologischen Daten in gestresst oder nicht gestresst klassifizieren.

Zuerst gilt es die im Datensatz gegebenen Sensordaten auf diejenigen zu reduzieren, welche auch mit Geräten wie einer Smartwatch auslesbar wären, für ein Beispiel siehe [2].
Die so ausgewählten Daten müssen dann noch bereinigt und vorverarbeitet werden. Methoden der Signalverarbeitung können nötig sein, um die zeitabhängigen Messungen für das maschinelle Lernen aufzubereiten. Gegebenenfalls ist der Datensatz für die spätere Evaluation in einen Trainings- und Test-Satz zu unterteilen.
Anschließend soll das Modell auf die Erkennung des Stress-Zustandes einer Person trainiert werden.
Dabei ist die Wahl aus verschiedenen Klassifizierern möglich, z.B. Deep Learning mit Convulation Neural Networks (CNNs), siehe [3].
Andere Varianten sind denkbar.
Im finalen Schritt wird das entstandene Modell evaluiert.

## Arbeitspakete

__1 - Konzeption__

Nach etwas Recherche und Einarbeitung soll ein Überblick des Lösungswegs entstehen. Eine erste Analyse der Trainingsdaten und Literatur ist ein guter Ausgangspunkt für das weitere Vorgehen. 
Der Workflow soll bereits möglichst detailliert beschrieben werden, über die Vorverarbeitung der Daten bis hin zum genutzten Modell und der angestrebten Evaluation.
Das Konzept kann noch kleinere Ungenauigkeiten enthalten, aber sollte insgesamt schlüssig sein.

__2 - Implementierung und Evaluation__

Basierend auf der konzeptionellen Ausarbeitung geht es nun an die Umsetzung. 
Zur Implementierung des gesamten Workflows empfiehlt sich Python unter Verwednung von Tensorflow [5] oder Pytorch [6] für CNNs. Auch hier können andere Ansätze gewählt werden.
Die Evaluation findet dann auf einem vorher abgetrennten Test-Datensatz statt. Das Modell soll auf dem Test-Satz anhand von relevanten Metriken bewertet werden.

__3 - Vortrag__

Eine 10-minütige Präsentation stellt abschließend das Projekt und den genommenen Lösungsweg zusammen mit den erreichten Ergebnissen vor.

## Literatur & Links

[1] Emotionserkennung (Paper) http://repositorio.utb.edu.co/handle/20.500.12585/8721  
[2] Bezogen auf Smartwatch (Paper) https://dl.acm.org/doi/abs/10.1145/3341162.3344831  
[3] Variante mit CNNs (Paper) https://ieeexplore.ieee.org/document/9669993  
[4] WESAD Datensatz https://ubicomp.eti.uni-siegen.de/home/datasets/icmi18/  
[5] TensorFlow https://www.tensorflow.org/  
[6] Pytorch https://pytorch.org/  
