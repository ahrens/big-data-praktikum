# Erkennung von Data Exfiltration via DNS auf Basis von DNS Logs

Betreuer: Martin Grimmer

Aufgabenstellung und Daten von: KPMG IT-Service GmbH

## Motivation
Im Jahr 2021 gaben 43% aller Unternehmen an, in den letzten zwölf Monaten Opfer einer Cyber-
Attacke gewesen zu sein. Aufgrund der steigenden Zahl an Fällen von Cyberkriminalität benötigt man
umfassende IT-Sicherheitsvorkehrungen, um sich vor Angriffen schützen zu können. Diese Angriffe
waren in letzter Zeit immer häufiger DNS basiert. Bei der Abwehr solcher Angriffe liegt der Fokus auf
Tunneling-Detection (Gegenmaßnahme zu DNS-Tunneling), dabei wird allerdings meist der Bereich
der DNS-Exfiltration-Malware übersehen, da dieser weitestgehend unbekannt war.

## Zielstellung
Es soll eine Möglichkeit zur Erkennung von Data Exfiltration via DNS auf Basis von DNS Logs
geschaffen werden. Diese DNS-Logs sind in einer Elasticsearch-Datenbank abgelegt und werden als
Export zur Verfügung gestellt. Das Ziel ist es, einen Machine-Learning Algorithmus zu entwickeln, der
eigenständig, mit einer angemessenen Laufzeit, Unregelmäßigkeiten in den DNS-Logs erkennt, die
auf Data Exfiltration hinweisen. Ein Vergleich (siehe Receiver Operating Characteristic Curve)
zwischen verschiedenen Classifiern (z.B. KNN, Decision Tree, Random Forest etc.) /
Optimierungsverfahren (Laufzeitoptimierung) ist wünschenswert. Dabei sollte die Laufzeit im
Verhältnis zur falsch-positiv bzw. falsch-negativ Rate dargestellt werden. Die Arbeitsergebnisse sollen
in einem Notebook festgehalten werden.

## Arbeitspakete

### 1. Anwendungsdesign und Lösungsskizze
Die in Elasticsearch abgelegten Daten, welche beispielhaft Data Exfiltration Beispiele enthalten,
müssen aufbereitet und überwacht werden. Es wird ein Machine-Learning Algorithmus benötigt, der
die DNS Files auf Anomalien untersucht. Dafür muss ein passendes Machine-Learning Modell
ausgesucht und passende Machine-Learning-Bibliotheken gefunden werden, in welchen der
entsprechende Algorithmus auf Basis der Daten trainiert und dessen Klassifikationsgenauigkeit
evaluiert werden kann. Es ist sich mit dem Datensatz vertraut zu machen und eine Vorgehensweise 
incl. Lösungsskizze anzufertigen.

### 2. Implementierung und Evaluierung
Das trainierte Modell innerhalb der Originaldaten möglichst viele DNS-Data-Exfiltration erkennen. Ein
weiteres Ziel wäre, dass der Algorithmus eine möglichst geringe falsch-positiv bzw. falsch-negativ
Rate aufweist. Das Verhältnis zwischen Laufzeit und falsch-positiv bzw. falsch-negativ Rate wird als
qualitatives Merkmal zur Bewertung des Algorithmus herangezogen.
Zusätzlich soll das Modell anhand eines weiteren Datensatzes getestet werden, damit man Probleme
wie Over- bzw. Underfitting ausschließen kann.

### 3. Vortrag
Ausarbeit einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze
(kurze Beschreibung des Algorithmus und der Programmiersprache, Nennung der verwendeten
Bibliotheken) und die Ergebnisse der Arbeit dargestellt werden.
