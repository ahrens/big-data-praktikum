# Optimierung verteilter Graphanalyse mittels Graphpartitionierung

**Voraussetzung**: Java-Programmierkenntnisse

## Motivation
Graphen sind ein intuitives Datenmodell um komplexe Beziehungen zwischen Entitäten aus realen
Szenarios zu modellieren und zu analysieren. Ein typisches Beispiel für einen Graphen ist ein
soziales Netzwerk, wo Nutzer als Knoten und Freundschaftsbeziehungen oder Nachrichten als Kanten
abgebildet werden.

Mit [Gradoop](https://github.com/dbs-leipzig/gradoop) wurde ein Framework entwickelt, mit dem sehr 
große Graph-Datensätze (im TB Bereich) flexibel analysiert werden können. Das System basiert auf der
verteilten In-Memory System [Apache Flink](https://flink.apache.org/) und nutzt dessen DataSet-API
um einen Graphen zu lesen, ihn zu verarbeiten/analysieren und das Ergebnis wieder zu persistieren.
Die Verarbeitung erfolgt dabei verteilt (eng. distributed), d.h., mehrere physische Maschinen (PCs)
teilen sich die Verarbeitung untereinander. Je günstiger bzw. geeigneter die Partitionierung der Daten
auf den Maschinen ist, desto weniger Kommunikationsaufwand besteht zwischen den einzelnen 
Maschinen und desto geringer ist die Laufzeit der Analyse.

Um die Performanz einer mit Gradoop erstellten Anwendung zu messen, wird die Laufzeit eines
Analyse-Workflows gemessen. Bei Erhöhung der Anzahl der Maschinen nimmt typischerweise auch 
die Laufzeit ab. Dieser sogenannte Speedup lässt sich in einem Chart visualisieren.

A: ![Runtime](images/03_runtime.png) B: ![Speedup](images/03_speedup.png)

**A** zeigt die Veränderung der Laufzeit bei zunehmender Anzahl der Maschinen für verschiedene Analysen.
**B** zeigt die gleichen Werte als Speedup, was den Faktor der Verbesserung der Laufzeit repräsentiert. 
Je näher der Speedup-Wert an dem linearen Verlauf ist, desto besser skaliert das System. Zu beachten
ist aber auch, dass mehr Maschinen auch mehr Kommunikation benötigen - man erkennt eine Verringerung
des Anstiegs bie 8 und 16 Maschinen.

## Zielstellung

Die Laufzeit und die horizontale Skalierbarkeit von Gradoop hängt von verschiedenen Faktoren ab. 
Ein Faktor ist die Partitionierung eines Graphen, d.h., welche Knoten und Kanten liegen zur 
Verarbeitung auf welcher Maschine. Apache Flink wählt innerhalb eines Optimizers eine bestimmte 
Partitionierung aus, die für den Workflow am besten geeignet erscheint. Es kann jedoch auch manuell
eine Partitionierung ausgewählt werden, was in der [API-Dokumentation](https://javadoc.io/doc/org.apache.flink/flink-java/latest/org/apache/flink/api/java/DataSet.html) beschrieben ist. 
Bspw. kann über die Funktionen [**rebalance**](https://nightlies.apache.org/flink/flink-docs-master/docs/dev/dataset/transformations/#rebalance), [**partitionByHash**](https://nightlies.apache.org/flink/flink-docs-master/docs/dev/dataset/transformations/#hash-partition), [**partitionByRange**](https://nightlies.apache.org/flink/flink-docs-master/docs/dev/dataset/transformations/#range-partition) und [**partitionCustom**](https://nightlies.apache.org/flink/flink-docs-release-1.13/api/java/)
ausgewählt werden, wie der entsprechende Datensatz partitioniert werden soll.

Ziel ist es für 3 ausgewählte Benchmarks zu untersuchen, welche Partitionierung geeignet sein könnte
und wie diese die Laufzeit und Skalierbarkeit beeinflusst (positiv oder negativ). Es existiert bereits ein [Benchmarking-Tool](https://github.com/dbs-leipzig/gradoop-benchmarks)
für Gradoop, mit dem die Laufzeit von den Benchmarks gemessen werden kann.

Zu untersuchende Workflows:
* [Snapshot Operator](https://github.com/dbs-leipzig/gradoop-benchmarks/blob/master/src/main/java/org/gradoop/benchmarks/tpgm/SnapshotBenchmark.java)
* [Difference Operator](https://github.com/dbs-leipzig/gradoop-benchmarks/blob/master/src/main/java/org/gradoop/benchmarks/tpgm/DiffBenchmark.java)
* [Pattern Matching Operator](https://github.com/dbs-leipzig/gradoop-benchmarks/blob/master/src/main/java/org/gradoop/benchmarks/tpgm/PatternMatchingBenchmark.java)

Dies kann zunächst lokal auf Ihrem PC getestet werden, die Evaluation soll dann aber auf unserem 
verteilten Cluster BDClu oder Athena erfolgen, welche jeweils aus 16 Maschinen bestehen.

Entsprechende Datensätze für die Evalaution bekommen Sie vom Betreuer.


## Arbeitspakete

__1 - Anwendungsdesign und Lösungsskizze__

Zur Lösung des Problems ist es notwendig sich mit [Gradoop](https://github.com/dbs-leipzig/gradoop/wiki) sowie [Apache Flink](https://flink.apache.org/) und dessen [Dataset API](https://nightlies.apache.org/flink/flink-docs-release-1.9/dev/batch/) vertraut zu machen.
Darüber hinaus sollte die Funktionsweise von [Graph-Partitionierung](https://en.wikipedia.org/wiki/Graph_partition) und die der oben genannten Operatoren verstanden werden.

Es ist eine Lösungsskizze anzufertigen, in der sie Zunächst die Operatoren und deren Workflows 
beschreiben. Dann beschreiben Sie die Funktionsweise der **partitionBy...** Methoden und wie 
diese genutzt werden könnten, um den Graph optimaler zu partitionieren.
Anhand eines Minimalbeispiels soll dies verdeutlicht werden.
Erstellen Sie weiterhin eine Architekturskizze die verdeutlicht, wie die Evaluation durchgeführt wird.

__2 - Implementierung und Evaluation__

Basierend auf der Lösungsskizze sind die Partitionierungen prototypisch in Gradoop selbst 
oder dem Gradoop-Benchmark zu implementieren.
Die Implementierung findet in einem Feature-Branch von Gradoop bzw. Gradoop-Benchmark statt. 
Bitte Forken sie zunächst beide Repositories auf Github in ihr privates Profil. Erstellen Sie 
dann einen neuen Branch ausgehend
vom `develop` (bzw. `master` bei gradoop-benchmark) nach Absprache mit dem Betreuer. 
In diesem Branch findet die Entwicklung statt.

Die Evaluation der Implementierung erfolgt im Anschluss auf unserem verteilten Rechencluster BDClu oder Athena.
Dabei wird die Auswirkung ihrer Implementierung auf die Laufzeit und horizontale Skalierbarkeit untersucht.

__3 - Vortrag__

Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze
(kurze Beschreibung der Partitionierungen, Nennung der verwendeten API-calls) und die Evaluationsergebnisse vorgestellt werden.


## Literatur und Links

- [System-Übersicht von Gradoop](https://dbs.uni-leipzig.de/file/Rost2021_Article_DistributedTemporalGraphAnalyt.pdf)
- [Slides Übersicht Gradoop](https://indico.scc.kit.edu/event/460/contributions/5772/attachments/2873/4171/gradoop_gridka19.pdf)
- [Apache Flink DataSet API](https://nightlies.apache.org/flink/flink-docs-release-1.14/docs/dev/dataset/overview/)
- [Gradoop Wiki](https://github.com/dbs-leipzig/gradoop/wiki)
- [Gradoop Getting Started](https://github.com/dbs-leipzig/gradoop/wiki/Getting-started)
