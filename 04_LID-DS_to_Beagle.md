# Incident response and digital forensics for LID-DS

## Motivation

### Incident Response / IT-Forensik
Incident Response ist die Reaktion und die damit verbundenen Maßnahmen auf einen IT-Sicherheitsvorfall. 
IT-Forensik ist eine methodisch vorgenommene Datenanalyse auf Datenträgern und Computernetzen zur Aufklärung von IT-Vorfällen.

### Beagle 
Beagle ist ein Tool zur Unterstützung von Incident Response und IT-Forensik, das verschiedene Datenquellen und Logs in Graphen umwandelt.

![preview of beagle](https://github.com/yampelo/beagle/raw/master/docs/imgs/upload_to_graph.gif)

### LID-DS 
LID-DS ist ein Datensatz für die Systemcall basierte HIDS Forschung.

## Zielstellung
Ziel des Praktikums ist es, dass die im LID-DS (2021 Version) enthaltenen Informationen von Beagle dargestellt werden können.
Der LID-DS enthält: Systemcalls, Netzwerkdaten (PCAP), allgemeine Systemstatistiken (CPU usage etc.).
Es sollen dabei jedoch nicht die Systemcalls selbst dargestellt werden, sondern nur deren Auswirkungen auf das System.
Dabei ist das bereits von Beagle verwendeten Graphmodell zu nutzen.

## Arbeitspakete

### 1. Einarbeitung und Lösungsskizze
Zunächst soll in einem Konzept skizziert werden, wie Beagle in die Lage versetzt werden kann die im LID-DS enthaltenen Daten zu laden.
- Beagle bietet dafür Dokumentation auf Github an.
- LID-DS enthält auch Beispielcode zum Laden der Daten.

### 2. Implementierung und Evaluation
Basierend auf der Lösungsskizze ist die Lösung umzusetzten, so dass beliebige Aufzeichnungen im LID-DS Format (2021er Version) mit Beagle geöffnet werden können.

### 3. Vortrag
Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze und die Ergebnisse dargestellt werden. Gern auch mit einer Demonstration.

## Links

* [Beagle](https://github.com/yampelo/beagle)
* [LID-DS](https://github.com/LID-DS/LID-DS)
