# Graph Stream Grouping mit der DataStream API von Apache Flink

## Motivation

Graphen sind ein intuitives Datenmodell um komplexe Beziehungen zwischen Entitäten aus realen 
Szenarios zu modellieren und zu analysieren. Da sich diese Objekte und Beziehungen in der Realität 
über die Zeit verändern, verändern sich auch die modellierten Graphen bezogen auf deren 
Struktur und Inhalt. Beispielsweise kommen neue Knoten und Kanten hinzu, werden entfernt oder 
verändern sich hinsichtlich ihrer Attribute.

Die Analyse dieser zeitlichen Veränderung von Graphen ist ein wichtiger Bestandteil, um genauere 
Informationen zum Entstehungsprozess von enthaltenen Strukturen zu erfahren oder herauszufinden, wann
eine Beziehung erstmalig auftrat oder wie lange im Durchschnitt eine Beziehung gültig ist.

Hoch frequentielle Änderungen eines Graphen können als Graph Stream modelliert werden. Die Gruppierung/Zusammenfassung
eines solchen Graph Streams ist Bestandteil unserer Forschungen.

In einer kürzlich fertiggestellen Anwendung wurde eine solche Gruppierung auf der Table API von Apache Flink umgesetzt.

## Zielstellung

Ziel des Themas ist die Entwicklung einer verteilten Anwendung zur Grupperung von Graph Streams unter Verwendung der 
DataStream API von Apache Flink und dessen Vergleich zur bestehenden Implementierung mit der Table API.

Die Gruppierung wird mit 4 Parametern konfiguriert:

* __Vertex Grouping Keys__: Die Eigenschaften, anhand der Knoten des Graphen gruppiert werden.
* __Edge Grouping Keys__: Die Eigenschaften, anhand der Kanten zwischen gruppierten Knoten des Graphen gruppiert werden.
* __Vertex Aggregate Functions__: Aggregatfunktionen zur Berechnung von aggregierten Werten der gruppierten Knoten.
* __Edge Aggregate Functions__: Aggregatfunktionen zur Berechnung von aggregierten Werten der gruppierten Kanten.

Da wir uns im Streaming Modell befinden, bieten sich auch die Verwendung von Window-basierten Gruppierungen an, um bspw. 
die durschschnittliche Ausleihdauer eines Nextbike pro Stunde zu ermitteln.

Als Beispieldaten werden die Twitter-API sowie ein Citibike-Rental Datensatz verwendet.

Die Implementierung erfolgt in Anlehnung an die bestehende Implementierung mit der Table API. Zugriff zum Repo wird von dem Betreuer organisiert.

Die Evaluation wird auf unserem verteilten Cluster BDClu ausgeführt.

## Arbeitspakete

__1 - Anwendungsdesign und Lösungsskizze__

Zur Lösung des Problem ist es notwendig sich mit Apache Flink und dessen TableAPI und DataStream API vertraut zu machen.
Darüber hinaus sollte die Funktionsweise der Graph Gruppierung verstanden werden. Einarbeitung in die die bestehende 
Implementierung mit der Table API ist dazu notwendig. Ebenfalls kann für den Nicht-Streaming-Fall der 
[Temporal Graph Explorer](https://github.com/dbs-leipzig/temporal_graph_explorer) als Demo verwendet werden.

Es ist eine Lösungsskizze anzufertigen, die beschreibt, welche DataStream Transformationen wie zusammenspielen, 
um die Gruppierung zu ermöglichen.


__2 - Implementierung und Evaluation__

Basierend auf der Lösungsskizze ist die Anwendung prototypisch zu implementieren. Hierbei ist darauf zu achten, dass die zur 
Verfügung gestellten Testdaten sowie die Grouping-Parameter flexibel ausgetauscht/angepasst werden können. Der Funktionsumfang sollte 
identisch zur bestehenden Implementierung sein. Als Programmiersprache für wird Java verwendet. 

Die Evaluation erfolgt im Anschluss auf unserem verteilten Rechencluster. Dabei werden verschiedene Metriken, wie bspw. Durchsatz und Skalierbarkeit, beider Implementierungen verglichen.

__3 - Vortrag__

Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze 
(kurze Beschreibung der Architektur, Nennung der verwendeten Transformationen) und die Evaluationsergebnisse vorgestellt werden. 


## Literatur

- [Graph Stream Summarization](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8731583)
- [Graph Stream Analysis Framework Degree Project](https://www.diva-portal.org/smash/get/diva2:830662/FULLTEXT01.pdf)

## Links

- [Apache Flink Table API](https://ci.apache.org/projects/flink/flink-docs-stable/dev/table/tableApi.html)
- [Apache Flink DataStream API](https://ci.apache.org/projects/flink/flink-docs-release-1.12/dev/datastream_api.html)
- [Graph Stream Grouping (Referzenzimplementierung on Table API)](https://git.informatik.uni-leipzig.de/rost/graph-stream-grouping)
