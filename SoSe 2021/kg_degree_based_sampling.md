# Iterative degree based sampling of knowledge graphs for benchmark data generation

## Motivation
Integrating data from different sources is necessary to answer complex information needs. Finding nodes in knowledge graphs, which refer to the same real-world entities (Entity Resolution) is necessary step in this data integration process. However, challenging benchmarks for entity resolution in knowledge graphs are still missing. To create realistic benchmarking datasets, which are subgraphs of larger knowledge graphs it is necessary to maintain the node degree distribution of the original knowledge graph. Furthermore, it is necessary to identify non-trivial pairs in different sources to create challenging benchmarks.

## Goal
The goal of this task is to use the [GRADOOP](https://github.com/dbs-leipzig/gradoop) framework to create a benchmark data generator, that can aid in this process. An important element of this implementation task lies in implementing the [iterative degree-based sampling approach](http://www.vldb.org/pvldb/vol13/p2326-sun.pdf). The datasets that will be used are DBpedia, Wikidata, Yago, IMDB, TheMovieDB and TheTVDB. Using existing links between the datasets as gold standard, the
students have to find entity pairs, that are challenging for a matching system (e.g. differently written name of the same person across different data sources).

## Work packages
### 1 Concept 
Make yourself familiar with the main concepts, datasets and APIs. Conceptualize the pipeline to create the benchmark data generator.

### 2 Implementation
Implement the application. Bear in mind, that it should be scalable to large knowledge graphs, modular to enable future enhancements and general enough to support different datasets. 

### 3 Presentation
The presentation (10 min + 5 min discussion) should showcase how the benchmark generator works, as well as information about the resulting datasets
