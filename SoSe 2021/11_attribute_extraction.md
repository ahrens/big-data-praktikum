# Attribute extraction from eCommerce product descriptions

## Motivation
Product attributes are vital to e-Commerce as platforms need attribute details to make recommendations and customers need attribute information
to compare products and make purchase decisions. However, attribute information is often noisy
and incomplete because of the inevitable hurdles
posed to retailers by the extremely huge and complex e-Commerce attribute system. On the other hand, product titles which are carefully designed by retailers are packed tightly with details to highlight all important aspects of products.
## Goal
Given a set of product profiles presented as unstructured text data (containing information like titles, descriptions, and bullets), and
a set of pre-defined target attributes (e.g., brand, flavor, size), the objective is to extract corresponding attribute values from unstructured text.

## Working packages:

### Application design and solution sketch
First of all, suitable data sources for creating training and test data must be identified. See for example https://www.kaggle.com/dataturks/best-buy-ecommerce-ner-dataset/home as a starting point. Based on this, an application for the extraction of attributes from unstructured text data is to be designed.

### Implementation and evaluation
Based on the solution sketch the necessary ML algorithms must be implemented. For training and testing the available data and improved iteratively. Additional data sources can also be used for this purpose. It is important to ensure that the application is designed flexibly so that the data sources used can be exchanged easily.

### Presentation
The presentation (10 min + 5 min discussion) should elaborate on the task, the implementended solution (brief description of the algorithm / architecture, application, as well as naming the libraries used) and the results of the evaluation.

