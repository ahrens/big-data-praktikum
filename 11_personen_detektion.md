# Hintergrund  
Für das Living Lab wollen wir die Möglichkeiten von Computer Vision mit Hilfe von Open-Source-Modelle anschaulich machen.  
Computer Vision ist ein Fach- und Anwendungsbereich der Informatik, der darauf abzielt, technische Systeme zu entwickeln, die visuelle Daten verarbeiten und interpretieren können. In den vergangenen Jahren sind eine Vielzahl neuer Machine Learning Modelle in diesem Bereich entwickelt worden. Diese Modelle und entsprechende Datensätze sind immer öfter frei im Internet verfügbar. In Kombination mit eigener Hardware und selbst erstellten Datensätzen möchten wir eine Personendetektion und Posenklassifizierung für das Living Lab realisieren.  

# Aufgabenpakete  
- Erstellen einer Lösungsskizze von max. 4 Seiten. Beschreibung der verfolgten Ziele und der einzelnen Arbeitsschritte.
- Implementierung der Personendetektion in folgenden Schritten
	- Erstellen einer Anwendung zur Detektion von Personen in geradlinig verzeichneten Bildern mit Hilfe eines auszuwählenden Open-Source-Modells  
	- Anpassung des Modells mit dem FRIDA Datensatz, zur Verbesserung der Genauigkeit bei der Verwendung von krummlinig verzeichneten Personenbildern  (z.B. einer Fisheye-Kamera)  
	- Anpassung des Modells zur Klassifizierung von sitzenden Personen  
	- Erstellen eines eigenen Datensatzes im Living Lab  
- Evaluierung des Modells und der Anwendung  
- Präsentation der Ergebnisse

# Links
[FRIDA](https://ieeexplore.ieee.org/abstract/document/9959697)
[YOLO object detection model](https://huggingface.co/spaces/akhaliq/yolov7)
[Beispiel für das Anpassen eines Modells für Bilder einer Fisheye-Kamera](https://www.degruyter.com/document/doi/10.1515/cdbme-2019-0061/html)
