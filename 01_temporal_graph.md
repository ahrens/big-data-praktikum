# Film- und Serien Temporal Graph (Netflix, Disney+, Prime, Hulu)

![Graph Integration](images/01_moviegraph.png)

## Motivation
Graphen eignen sich um Beziehungen zwischen Entitäten zu modellieren. Entitäten, im Graph Kontext als 
Knoten (engl. Vertices) bezeichnet, können beispielsweise 
Filme, Serien, Schauspieler oder Regisseure sein, wobei Beziehungen, im Graph-Kontext als Kanten 
(engl. Edges) bezeichnet, die Interaktion dieser Entitäten 
darstellen, z.B. 'spielt in', 'produziert von'.
Betrachtet man die Entwicklung eines solchen Graphen über die Zeit, spricht man von einem Temporalen 
Graphen. Dieser beinhaltet die komplette Historie des Graphen, also konkret _wann_ Entitäten und 
deren Beziehungen entstanden sind und ggf. _wie lange_ diese gültig sind.
Die zusätzliche Zeitdimension ermöglicht deutlich vielfältigere Analysen, als die rein statische 
Betrachtung des Netzwerks zu einem Zeitpunkt. 

Auf Kaggle existieren verschiedene Datensätze zu Film und Serien auf bekannten Streaming-Portalen. 
Neben Erscheinungsdatum stehen auch weitere Informationen wie Director, Cast oder Genre zur 
Verfügung. Ein weiterer Datensatz beinhaltet Nutzer-Bewertungen für Netflix Serien und Filme. 
Ein letzter Datensatz der IMDB beinhaltet aggregierte Bewertungen.
Diese stark vernetzten Daten können als temporaler Knowledge-Graph modelliert werden. Dazu ist es 
nötig, ein geeignetes Graph-Schema zu entwerfen und die tabellarischen Daten in dieses Schema zu 
importieren. Ein geeignetes Tool zum Import dieser Daten in eine temporale Graph-Struktur stellt das
verteilte Graph-Analyse-Tool 
[Gradoop](https://github.com/dbs-leipzig/gradoop) dar. Gradoop bietet bereits 
[ETL-Werkzeuge](https://github.com/dbs-leipzig/gradoop/wiki/Data-Integration-Operators) zum Import an.
Weiterhin bietet Gradoop eine vielzahl von Analyse-Operatoren, die zu Analyse-Pipelines kombiniert
werden können.

## Zielstellung
Ziel des Praktikums ist es, die Daten der 5 gegebenen Datensätze zu einem temporalen Knowledge-Graphen 
zusammenzuführen. Dazu muss zunächst ein Graph-Schema anhand der in den Daten enthaltenen Attribute 
entwickelt werden. Dann werden unter Verwendung von 
Gradoop nacheinander die Datensätze in die entworfene Graph-Struktur geladen, bis am Ende der Pipeline ein 
fertiger Graph zur Verfügung steht. Dieser soll dann zunächst persistiert werden.

Auf den fertigen Graphen können nun Graph-Analysen ausgeführt werden, z.B., 

1. Statistiken über Filme, Schauspieler, Bewertung und deren Verbindung via Graph-Gurppierung. Z.B. was ist die durchschnittliche IMDB Bewertung von Filmen, wo Keanu Reeves mitgespielt hat? Wie hat sich die Bewertung über die Zeit verändert?
2. Film-Empfehlungen generieren anhand von Menge gegebener Filmen und Serien
3. Welcher Monat eignet sich am besten für den Release eines neuen Films einer gegebenen Kategorie?
4. Top-10 ähnliche Filme zu gegebenen Film
5. Am besten bewertete Filme für gegebenen Schauspieler
6. PageRank Score für Filme und Serien

Weiterhin sollten Sie sich 2 weitere interessante Analysen entwickeln. 
Das Resultat einer jeden Analyse kann z.B. mit Gephi oder GraphViz visualisiert werden.

## Arbeitspakete

### 1. Einarbeitung und Lösungsskizze
Zunächst soll in einem Konzept skizziert werden, wie und mit welchen Werkzeugen die Datensätze in ein Graphen überführt werden. Dazu ist es notwendig, sich einen Überblick über die Datensätze und deren Schema zu verschaffen. 
Stellen Sie zudem dar, wie gleiche Entitäten identifiziert werden sollen? Wie sieht das resultierende Graph-Schema aus? Unter der Annahme des fertigen temporalen Graphen: Wie können die beschriebenen Analysen umgesetzt werden? 
Was wären zwei denkbare eigene Analysen?

### 2. Implementierung und Evaluation
Basierend auf der Lösungsskizze ist der experimentelle Aufbau mit Gradoop umzusetzen. Es sollen zwei Implementierungen entstehen:
1. Daten-Importer: Lesen der Datensätze + Erstellen des Graphen
2. Graph-Analysen: Die Analysen sollen jeweils als Workflows definiert werden und das Ergebnis entsprechend visualisiert werden.

### 3. Vortrag
Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze und die Ergebnisse dargestellt werden.

## Links

* [Netflix data](https://www.kaggle.com/datasets/shivamb/netflix-shows)
* [Amazon Prime data](https://www.kaggle.com/datasets/shivamb/amazon-prime-movies-and-tv-shows)
* [Disney+ data](https://www.kaggle.com/datasets/shivamb/disney-movies-and-tv-shows)
* [Hulu data](https://www.kaggle.com/datasets/shivamb/hulu-movies-and-tv-shows)
* [Netflix Prize data (Ratings)](https://www.kaggle.com/datasets/netflix-inc/netflix-prize-data)
* [IMDB Ratings](https://www.imdb.com/interfaces/)
