# Synthetische Zeitreihen für Energieverbräuche erzeugen

## Motivation

Industrie 4.0 Anwendungen (z.B. Predictive Maintenance) benötigen Daten. Diese sind nicht immer in ausreichender Menge vorhanden. Deshalb wird nach Ansätzen gesucht, diese Daten künstlich zu erzeugen. 

![Wirkleistung von drei Anlagen](images/data.png)

## Zielstellung
Ziel des Praktikums ist es, synthetische Zeitreihen für Energieverbräuche (Wirkleistung) zu erzeugen, die den Real-Daten einer Produktionsanlage sehr ähnlich sind (siehe Abbildung). Die vorliegenden Real-Daten wurden aufgezeichnet und im JSON-Format gespeichert. 

## Arbeitspakete

### 1. Einarbeitung und Lösungsskizze
Zunächst soll in einem Konzept skizziert werden, mit welchen Methoden synthetische Zeitreihen erzeugt werden können. Aus der Literatur bekannte Ansätze (z.B. LSTM, Generative Models, Data Augmentation, ...) sind daher zunächst in der Theorie kurz vorzustellen (Methoden, Stärken & Schwächen hinsichtlich des vorliegenden Anwendungsfalls). 

### 2. Implementierung und Evaluation
Danach sind in Absprache mit dem Betreuer zwei Ansätze zu implementieren und die damit erzielten Ergebnisse zu evaluieren. 

### 3. Vortrag
Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze und die Ergebnisse dargestellt werden.

## Links

* [Long Short-Term Memory](https://www.bioinf.jku.at/publications/older/2604.pdf)
* [Boost short-term load forecast](https://energyinformatics.springeropen.com/articles/10.1186/s42162-022-00214-7)
* [Long Short-Term Memory implemented using Python](https://waliamrinal.medium.com/long-short-term-memory-lstm-and-how-to-implement-lstm-using-python-7554e4a1776d)
* [LSTM Example - Time Series Data](https://levelup.gitconnected.com/forecasting-walmart-quarterly-revenue-pytorch-lstm-example-b4e4b20862a7)

