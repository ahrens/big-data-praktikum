## Background and Motivation

![DAS cable at Rhônegletscher](images/Rhone_cable.png)

The last decade witnessed an explosion in yearly number of publications on passive glacier seismology (cryoseismology). The seismic signals from a wide range of glacier-related processes fill a broad band of frequencies (from 10−3 to 10² Hz) and moment magnitudes (from M–3 to M7) providing a fresh and unprecedented view on fundamental processes in the cryosphere. New insights into basal motion, iceberg calving, glacier, iceberg, and sea ice dynamics, and precursory signs of unstable glaciers and ice structural changes are being discovered with seismological techniques. These observations offer an invaluable foundation for understanding ongoing environmental changes and for future monitoring of ice bodies worldwide.

For the first time, the passive seismic instrumentation of an entire glacier close to the flow line was realized: State of the art DAS (distributed acoustic sensing) technology paved the ground to acquire seismic data with a fiber-optic cable over an extent of 9 km following a zig-zag pattern and covering Rhônegletscher (Swiss Alps) from its accumulation to its ablation zone. 
Rhônegletscher is a temperate and well accessible glacier located in the Swiss Alps, with a length of 9 km (~15.5 km2 total extension) and an average surface slope of 10°, ranging in elevation between 3600 to 2200 m above sea level (a.s.l.) (GLAMOS, 2020). In contrast to classical array data sets using individual point sensors (geophones or seismometers), data on Rhônegletscher were collected using distributed acoustic sensing (DAS) technology (Walter et al., 2020; Hudson et al., 2020). DAS measurements are based on an interrogator emitting a sequence of „outgoing“ laser pulses into an optical fiber. The light experiences scattering at inhomogeneities along the cable and is recorded as an „incoming“ pulse back at the interrogator. Phase shifts of the backscattered light provide a highly sampled time series of tiny cable strains, such as those induced by passing seismic signals. Seismic recordings can thus be obtained every few meters along the cable offering the possibility to record incident seismic waves with frequency sensitivity ranging from millihertz to kilohertz (Farhadiroushan et al., 2009, Parker et al., 2014). In the course of this experiment about 16 TB of cryoseismological records from > 4000 DAS channels along the 9 km long fiber-optic cable were acquired during one month in spring 2020.

## Goal

In this project we would like to apply signal processing techniques (fourier transformation and wavelet transformation) to the DAS data set from Rhônegletscher. The results will be visualized using an Earth System Data Cube (*.zarr format) using the data cube viewer Lexcube (lexcube.org) for visual inspection of the frequency-dependent time and space domains. We will further use the cubes for Machine Learning (e.g. unsupervised cluster analysis).


## Work packages


###	1. Orientation and solution approach

* getting familiar with the domain, the data and the existing scripts
* conceptual draft


	
	
###	2. Implementation and Evaluation


* improve existing scripts and pipelines for the computation of spectrograms and scalograms in the format *.zarr on the SC cluster
	
	* combine and reduce to one intuitive (user-friendly) pipeline if possible
	* replace hard coded variables 
	* enhance overall efficiency if possible
	* improve data access: enable data streaming
	
* Visualization of results using lexcube


	
###	3. Presentation 


* 10 min talk and presentation (slides) demonstrating the topic/motivation, the solution approach, the results
